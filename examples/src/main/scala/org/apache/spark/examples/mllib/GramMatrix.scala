package org.apache.spark.examples.mllib

import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.linalg.distributed.{IndexedRow, IndexedRowMatrix}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by luna on 9/15/16.
  */
object GramMatrix {
  def main(args: Array[String]) {
    if (args.length < 3) {
      System.err.println("Usage: GrammMatrix <input file> <blocksize> <size>")
      System.exit(1)
    }

    val blocksize=args(1).toInt
    val size=args(2).toLong

    val conf = new SparkConf().setAppName("GrammMatrix")
    val sc = new SparkContext(conf)

    //Load and parse the data file.
    println(s"==========partition: ${Math.pow(Math.ceil(size/blocksize), 2).toInt}")
    val dataRDD = sc.textFile(args(0), Math.pow(Math.ceil(size/blocksize), 2).toInt).
      map(line => Vectors.dense(line.split(" ").map(_.toDouble))).zipWithIndex.map(_.swap)

    val rows = dataRDD.map{ case(k,v)=>IndexedRow(k,v) }

    val mat = new IndexedRowMatrix(rows)

    val blockMat=mat.toBlockMatrix(blocksize,blocksize)

    // Compute SVD.
    val ata=blockMat.transpose.multiply(blockMat)
    //println(ata.toIndexedRowMatrix().rows.first().vector(9))

    ata.blocks.saveAsTextFile("/lustre/atlas/scratch/ul6/csc209/tmp/ata")

    sc.stop()
  }

}
